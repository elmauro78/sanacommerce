namespace SanaCommerce.Migrations
{
    using System.Data.Entity.Migrations;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<SanaCommerce.Models.SanaCommerceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SanaCommerce.Models.SanaCommerceContext context)
        {
            context.Products.AddOrUpdate(x => x.id,
                new Product() { id = 1, product_number = "10001", title = "Product 1", price = 10.10M },
                new Product() { id = 1, product_number = "10002", title = "Product 2", price = 20.10M },
                new Product() { id = 1, product_number = "10003", title = "Product 3", price = 30.10M }
            );
        }
    }
}
