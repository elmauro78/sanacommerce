﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SanaCommerce.Models
{
    public class Product
    {
        public int id { get; set; }
        [Required]
        public string product_number { get; set; }
        public string title { get; set; }
        public decimal price { get; set; }
    }
}