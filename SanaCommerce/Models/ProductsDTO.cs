﻿namespace SanaCommerce.Models
{
    public class ProductDTO
    {
        public int id { get; set; }
        public string product_number { get; set; }
        public string title { get; set; }
        public decimal price { get; set; }
    }
}