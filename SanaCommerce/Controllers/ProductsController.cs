﻿using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SanaCommerce.Models;
using System.Web.Http.Description;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System;

namespace SanaCommerce.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsController : ApiController
    {
        static IModelMaster model;

        [Route("mode/{mode}")]
        public IHttpActionResult SetModel(String mode)
        {
            if (mode == "XML")
            {
                model = new SanaCommerceXMLModel();
            }
            else {
                model = new SanaCommerceModel();
            }

            var message = new
            {
                value = mode,
                success = "true"
            };

            return new TextResult(HttpStatusCode.OK, Request, message);
        }

        // GET: api/products
        public IHttpActionResult GetProducts()
        {
            var products = model.GetProducts<Product>();

            if (products.Count() == 0)
            {
                return new TextResult(HttpStatusCode.NotFound, Request, null);
            }
            else
            {
                var message = new
                {
                    products = products,
                    success = "true",
                    total_elements = products.Count()
                };

                return new TextResult(HttpStatusCode.OK, Request, message);
            }
        }

        // GET: api/Products/5
        [Route("{id:int}")]
        [ResponseType(typeof(Product))]
        IHttpActionResult GetProduct(int id)
        {
            Product product = model.GetProduct(id);
            if (product == null)
            {
                return new TextResult(HttpStatusCode.NotFound, Request, null);
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        [Route("{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return new TextResult(HttpStatusCode.BadRequest, Request, null);
            }

            if (id != product.id)
            {
                return new TextResult(HttpStatusCode.BadRequest, Request, null);
            }

            try
            {
                await model.PutProduct(id, product);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!model.ProductExists(id))
                {
                    return new TextResult(HttpStatusCode.NotFound, Request, null);
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return new TextResult(HttpStatusCode.BadRequest, Request, null);
            }

            await model.PostProduct(product);

            return CreatedAtRoute("DefaultApi", new { id = product.id }, product);
        }

        // DELETE: api/Products/5
        [Route("{id:int}")]
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            Product product = model.GetProduct(id);
            if (product == null)
            {
                return new TextResult(HttpStatusCode.NotFound, Request, null);
            }

            await model.DeleteProduct(id);

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                model.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}