﻿var service = function (id, name) {
    var self = this;
    self.id = ko.observable(id);
    self.name = ko.observable(name);
};

var ViewModel = function () {
    var self = this;
    var productsUri = '/api/products/';

    self.products = ko.observableArray();
    self.error = ko.observable();
    self.visibility = ko.observable();
    self.visible = ko.observable();
    self.show = ko.observable(false);
    self.loader = ko.observable(false);
    self.setMode = ko.observable();

    self.services = ko.observableArray([]);
    self.addservice = function (service) {
        self.services.push(service);
    };
    self.selectedservice = ko.observable();
    self.selectedservice.subscribe(function () {
        var selectd = self.selectedservice();
        var mode;

        switch (selectd) {
            case '1': mode = 'XML';
                break;

            case '2': mode = 'DATABASE';
                break;

            default: mode = 'XML';
                break;
        }
        self.setMode(mode);
    });

    self.addservice(new service('1', 'XML'));
    self.addservice(new service('2', 'Database'));
    
    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "Authorization": "Basic bXlfdXNlcjpteV9wYXNzd29yZA=="
            },
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.loader(false);
            self.products([]);
            self.error(errorThrown);
        });
    }

    self.setMode = function (mode) {
        ajaxHelper(productsUri + 'mode/' + mode, 'POST').done(function () {
            self.loader(true);
            getAllProducts();
        });
    }

    function getAllProducts() {
        ajaxHelper(productsUri, 'GET').done(function (data) {
            self.products(data.products);
            self.loader(false);

            self.show(false);
            self.visible(false);

            self.hideVisibility();
        });
    }

    // Fetch the initial data.
    // setMode();
    // getAllProducts();

    self.removeProduct = function (item) {
        ajaxHelper(productsUri + item.id, 'DELETE').done(function () {
            getAllProducts();
        });
    }

    self.setVisibility = function () {
        self.visibility(true);
        self.show(false);
        self.visible(false);

        self.currentProduct.id = 0;
        self.currentProduct.product_number('');
        self.currentProduct.title('');
        self.currentProduct.price('');
    }

    self.hideVisibility = function () {
        self.visibility(false);
        self.show(false);
        self.visible(false);

        self.currentProduct.id = 0;
        self.currentProduct.product_number('');
        self.currentProduct.title('');
        self.currentProduct.price('');
    }

    self.currentProduct = {
        id: 0,
        product_number: ko.observable(),
        title: ko.observable(),
        price: ko.observable()
    }

    self.editProduct = function (item) {
        self.visibility(true);
        self.show(false);
        self.visible(false);

        self.currentProduct.id = item.id;
        self.currentProduct.product_number(item.product_number);
        self.currentProduct.title(item.title);
        self.currentProduct.price(item.price);
    }

    self.addProduct = function (formElement) {
        var product = {
            id: self.currentProduct.id,
            product_number: self.currentProduct.product_number(),
            title: self.currentProduct.title(),
            price: self.currentProduct.price()
        };

        if (product.id !== 0) {
            ajaxHelper(productsUri + product.id, 'PUT', product).done(function () {
                getAllProducts();
            });
        }
        else{
            ajaxHelper(productsUri, 'POST', product).done(function (item) {
                getAllProducts();
            });
        }
    }
};

ko.applyBindings(new ViewModel());