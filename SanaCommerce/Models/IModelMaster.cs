﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SanaCommerce.Models
{
    public interface IModelMaster
    {
        List<ProductDTO> GetProducts<Product>();
        Product GetProduct(int id);
        Task<int> PutProduct(int id, Product product);
        Task<int> PostProduct(Product product);
        Task<int> DeleteProduct(int id);
        bool ProductExists(int id);
        void Dispose();
    }
}