﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.XPath;

namespace SanaCommerce.Models
{
    public class SanaCommerceXMLModel : IModelMaster
    {
        string strFileName = HttpContext.Current.Server.MapPath("~/App_Data/Products.xml");

        public Task<int> DeleteProduct(int id)
        {
            try
            {
                if (File.Exists(strFileName))
                {
                    XmlDocument objXmlDocument = new XmlDocument();
                    objXmlDocument.Load(strFileName);

                    XmlNode node = objXmlDocument.SelectSingleNode("//Product[id='" + id + "']");

                    if (node != null)
                    {
                        objXmlDocument.ChildNodes[1].RemoveChild(node);
                    }

                    Task<int> task = new Task<int>(() =>
                    {
                        objXmlDocument.Save(strFileName);
                        return 1;
                    });
                    task.Start();
                    return task;
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist in the folder");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
        }

        public Product GetProduct(int id)
        {
            try
            {
                if (File.Exists(strFileName))
                {
                    Product objProduct = new Product();

                    XPathDocument doc = new XPathDocument(strFileName);
                    XPathNavigator nav = doc.CreateNavigator();
                    XPathNodeIterator iterator;

                    iterator = nav.Select("//Product[id='" + id + "']");

                    while (iterator.MoveNext())
                    {
                        XPathNavigator nav2 = iterator.Current.Clone();

                        objProduct.id = Convert.ToInt32(nav2.Select("//Product").Current.SelectSingleNode("id").InnerXml);
                        objProduct.product_number = nav2.Select("//Product").Current.SelectSingleNode("product_number").InnerXml;
                        objProduct.title = nav2.Select("//Product").Current.SelectSingleNode("title").InnerXml;
                        objProduct.price = Convert.ToInt64(nav2.Select("//Product").Current.SelectSingleNode("price").InnerXml);
                    }
                    return objProduct;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public List<ProductDTO> GetProducts<Product>()
        {
            try
            {
                if (File.Exists(strFileName))
                {
                    // Loading the file into XPath document
                    XPathDocument doc = new XPathDocument(strFileName);
                    XPathNavigator nav = doc.CreateNavigator();

                    XPathExpression exp = nav.Compile("/Products/Product"); // Getting all products

                    // Sorting the records by Product Id
                    exp.AddSort("product_number", System.Xml.XPath.XmlSortOrder.Ascending, System.Xml.XPath.XmlCaseOrder.None, "", System.Xml.XPath.XmlDataType.Text);

                    XPathNodeIterator iterator = nav.Select(exp);
                    IList<ProductDTO> objProducts = new List<ProductDTO>();
                    var query = objProducts.AsQueryable();

                    while (iterator.MoveNext())
                    {
                        XPathNavigator nav2 = iterator.Current.Clone();

                        ProductDTO objProduct = new ProductDTO();
                        objProduct.id = Convert.ToInt32(nav2.Select("//Product").Current.SelectSingleNode("id").InnerXml);
                        objProduct.product_number = nav2.Select("//Product").Current.SelectSingleNode("product_number").InnerXml;
                        objProduct.title = nav2.Select("//Product").Current.SelectSingleNode("title").InnerXml;
                        objProduct.price = Convert.ToInt64(nav2.Select("//Product").Current.SelectSingleNode("price").InnerXml);
                        objProducts.Add(objProduct);
                    }

                    var products = from p in query
                    select new ProductDTO()
                    {
                        id = p.id,
                        product_number = p.product_number,
                        title = p.title,
                        price = p.price
                    };

                    return products.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Enumerable.Empty<ProductDTO>().ToList();
        }

        public Task<int> PostProduct(Product product)
        {
            try
            {
                // Checking if the file exist
                if (!File.Exists(strFileName))
                {
                    // If file does not exist in the database path, create and store an empty Products node
                    XmlTextWriter textWritter = new XmlTextWriter(strFileName, null);
                    textWritter.WriteStartDocument();
                    textWritter.WriteStartElement("Products");
                    textWritter.WriteEndElement();
                    textWritter.Close();
                }

                // Create the XML docment by loading the file
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strFileName);

                // Creating Product node
                XmlElement subNode = xmlDoc.CreateElement("Product");

                // Getting the maximum Id based on the XML data already stored
                string strId = CommonMethods.GetMaxValue(xmlDoc, "Products" + "/" + "Product" + "/" + "id").ToString();

                // Adding Id column. Auto generated column
                subNode.AppendChild(CommonMethods.CreateXMLElement(xmlDoc, "id", strId));
                xmlDoc.DocumentElement.AppendChild(subNode);

                // Adding Product column. Note - The employee Id is build automatically. No input required
                subNode.AppendChild(CommonMethods.CreateXMLElement(xmlDoc, "product_number", product.product_number));
                xmlDoc.DocumentElement.AppendChild(subNode);

                subNode.AppendChild(CommonMethods.CreateXMLElement(xmlDoc, "title", product.title));
                xmlDoc.DocumentElement.AppendChild(subNode);

                subNode.AppendChild(CommonMethods.CreateXMLElement(xmlDoc, "price", product.price.ToString()));
                xmlDoc.DocumentElement.AppendChild(subNode);

                // Saving the file after adding the new product node
                Task<int> task = new Task<int>(() =>
                {
                    xmlDoc.Save(strFileName);
                    return 1;
                });
                task.Start();
                return task;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ProductExists(int id)
        {
            throw new NotImplementedException();
        }

        public Task<int> PutProduct(int id, Product product)
        {
            try
            {
                if (File.Exists(strFileName))
                {
                    XmlDocument objXmlDocument = new XmlDocument();
                    objXmlDocument.Load(strFileName);

                    // Getting a particular Product by selecting using Xpath query
                    XmlNode node = objXmlDocument.SelectSingleNode("//Product[id='" + product.id + "']");

                    if (node != null)
                    {
                        // Assigining all the values
                        node.SelectNodes("product_number").Item(0).FirstChild.Value = product.product_number;
                        node.SelectNodes("title").Item(0).FirstChild.Value = product.title;
                        node.SelectNodes("price").Item(0).FirstChild.Value = product.price.ToString();
                    }
                    // Saving the file

                    Task<int> task = new Task<int>(() =>
                    {
                        objXmlDocument.Save(strFileName);
                        return 1;
                    });
                    task.Start();
                    return task;
                }
                else
                {
                    Exception ex = new Exception("Database file does not exist in the folder");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}