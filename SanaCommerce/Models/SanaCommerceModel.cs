﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SanaCommerce.Models
{
    public class SanaCommerceModel : IModelMaster
    {
        private SanaCommerceContext db = new SanaCommerceContext();

        public void Dispose()
        {
        }

        public List<ProductDTO> GetProducts<Product>()
        {
            var products = from p in db.Products
            select new ProductDTO()
            {
                id = p.id,
                product_number = p.product_number,
                title = p.title,
                price = p.price
            };
            return products.ToList();
        }

        public Product GetProduct(int id)
        {
            return db.Products.Find(id);
        }

        public async Task<int> PutProduct(int id, Product product)
        {
            db.Entry(product).State = EntityState.Modified;
            return await db.SaveChangesAsync();
        }

        public async Task<int> PostProduct(Product product)
        {
            db.Products.Add(product);
            return await db.SaveChangesAsync();
        }

        public async Task<int> DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            return await db.SaveChangesAsync();
        }
        public bool ProductExists(int id)
        {
            return db.Products.Count(e => e.id == id) > 0;
        }
    }
}